@merge jte {
    allow_scm_jenkinsfile = false
    permissive_initialization = false
    reverse_library_resolution = false
}

@merge libraries {
    git {
        gitlab{
        connection = 'gitLabConnection'
        job_name = 'service-account'
        }
    }
    sdp{
        images{
            registry = 'https://docker.pkg.github.com'
            repository = 'boozallen/sdp-images'
            cred = 'github'
        }
    }
    kiuwan
    nexus
    @override ansible
    serenity
    utility
}

stages {
    
}

@merge keywords {
    develop = ~/^[Dd]ev(elop|elopment|eloper|)$/
    main = ~/^[Mm](ain|aster)$/
    tmp = "tmp"
}

@merge application_environments {
    test {
        short_name = "DEV"
        long_name = "Development"
        @override branch_name = "development"
        @override notify_to = ["developer@popular.com"]
    }
    prd {
        short_name = "PRD"
        long_name = "Production"
        branch_name = "main"
        notify_to = ["developer@popular.com", "supervisor@popular.com"]
    }
}

template_methods{
  email_notification
}
