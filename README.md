# MBTI: Jenkins Templating Engine
This project acts as JTE pipeline template as part of a POC related to [MBTI-122](https://delivery.popular.com/browse/MBTI-122) Jira story<br/>
See: <https://jenkinsci.github.io/templating-engine-plugin/2.5.3/>

## Appendix
The Templating Engine Plugin allows you to remove the Jenkinsfile from each repository by defining a common workflow for teams to inherit. Instead of an entire pipeline definition in each repository, teams supply a configuration file specifying which tools to use for the workflow.


## Templates
We have worked on some pipelines templates based on [Pipelines Template concept](https://jenkinsci.github.io/templating-engine-plugin/2.5.3/concepts/pipeline-templates/) from JTE framework.<br/>
See: <https://jenkinsci.github.io/templating-engine-plugin/2.5.3/concepts/pipeline-templates/pipeline-catalog/><br/><br/>
Below we are detailing each available template.

<details>
<summary title="click to show Andromeda Template details">

## _Andromeda Template_

</summary>
This template provides a pipeline with a large number of stages.<br/>
This pipeline template uses a branch strategy to run stages.

#### Default stages
The following stages are available by default in this pipeline template:
 1. **Prepare Pipeline**: This stage is charged with applying some initial configuration to the pipeline.
 2. **Build**: This stage builds the application.
 3. **Unit Test**: This stage run the application UTs.
 4. **Unit Integration Test**: This stage run the application Unit ITs.
 5. **Smart Code Coverage**: This stage run the application Smart code coverage analysis.
 6. **Static Code Analysis**: This stage runs the vulnerability scan based on a Kiuwan analysis.
 7. **Publish Artifacts**: This stage publish the artifacts to a remote repository (for now only to Nexus).
 8. **Deploy To \<ENVIRONMENT>**: This stage aims to deploy the application to x environment. 
 The deployment is managed by Ansible, where the control node deploys the application 
 to Tomcat servers. In our roadmap we have planned to add support to Android and IOS applications deployment, 
 who's not strictly deploying to a Tomcat server.
 9. **Service Testing**: This stage is part of the post deployment actions and performs some service validation.
 10. **User Interface Testing**: This stage performs some UI Tests. This stage is part of the post deployment actions too.
 #### Optional stages
 The following stages are available as optional stages in this pipeline template:
 1. **Notification**: This stage will always run to notify (now just by email, but other channels can be added) the pipeline execution.
 status and possible details.

 #### Graphic visualization
 Below we are showing some visual representation for each branch and brach action.

 - Pipeline executions on a commit to a **feature** branch, run: **Prepare pipeline**, and all 
 continuous integration (**Build**, **Unit Test**, **Unit Integration Test**, **Smart Code Coverage** and **Static Code Analysis**) stages. Additional, you can add the optional **Notification** stage.
 ![alt text](img/andromeda_template_commit_feature.png "when commit to feature branch")
 ---
 - Pipeline executions on a commit to **develop** branch run: **Prepare pipeline**, all continuous integration stages, all continuous delivery (**Publish Artifacts**, **Deploy To**) stages and all post deployment (**Service Testing**, **User Interface Testing**) stages. Additional, you can add the optional **Notification** stage.
 ![alt text](img/andromeda_template_commit_develop.png "when commit to develop branch")
 ---
 - Pipeline executions on a merge request to **develop** or **master** branch run: **Prepare pipeline** and all continuous integration stages. <br/>Additional, you can add the optional **Notification** stage.
 ![alt text](img/andromeda_template_commit_feature.png "when merge request to develop/master branch")
 ---
 - Pipeline executions on a merge to **master** branch, run: **Prepare pipeline**, all continuous delivery stages and all post deployment stages. <br/>Additional, you can add the optional **Notification** stage.
 ![alt text](img/andromeda_template_mr_master.png "when merge to master branch")
 ---

#### Template Implementation
To use this template as your pipeline template, just follow the next steps:

 1. Be sure that your git project includes a **pipeline_config.groovy** file at the root level.
 2. Add the following code to your **pipeline_config.groovy** file:
 ```groovy
jte {
    pipeline_template = 'andromeda_template'
} 
 ```

Below is an example (based on a maven app) of how an application can implement the **Andromeda template** using its **pipeline_config.groovy** fille.

```groovy
jte {
    pipeline_template = 'andromeda_template'
}

libraries {
    maven
    ansible {
        deployment_type = 'tomcat'
    }
    notification
}

application_environments {
    test {
        notify_to = ['developer@popular.com', 'tester@popular.com']
    }
}
```

#### Template required libraries
This template handles Maven and Gradle-based projects.<br/>
You need to specify your preference adding the 
**maven** or **gradle** library to your **pipeline_config.groovy** file as follow: 
```groovy
libraries {
    maven
 }
```
 If you need to use gradle, just remove maven and add gradle.

 #### Template optional libraries
 Some libraries are optional to pass to your **pipeline_config.groovy** file. For example,
 you can add the **notification** library to activate the optional pipeline **Notification** stage as follow.<br/>
```groovy
libraries {
    maven
    notification
}
```

</details>

<details>
<summary title="click to show Delphinus Template details">

## _Delphinus Template_

</summary>
This Template provides a pipeline with a large number of stages where only the notification stage is optional.<br/>
This template is intended to be used only in test environment to tests all flows/stages.

#### Default stages
The following stages are available by default in this pipeline template:
 1. **Prepare Pipeline**: This stage is charged with applying some initial configuration to the pipeline.
 2. **Build**: This stage builds the application.
 3. **Unit Test**: This stage run the application UTs.
 4. **Unit Integration Test**: This stage run the application Unit ITs.
 5. **Smart Code Coverage**: This stage run the application Smart code coverage analysis.
 6. **Static Code Analysis**: This stage runs the vulnerability scan based on a Kiuwan analysis.
 7. **Publish Artifacts**: This stage publish the artifacts to a remote repository (for now only to Nexus).
 8. **Deploy To \<ENVIRONMENT>**: This stage aims to deploy the application to x environment. 
 The deployment is managed by Ansible, where the control node deploys the application 
 to Tomcat servers. In our roadmap we have planned to add support to Android and IOS applications deployment, 
 who's not strictly deploying to a Tomcat server.
 9. **Service Testing**: This stage is part of the post deployment actions and performs some service validation.
 10. **User Interface Testing**: This stage performs some UI Tests. This stage is part of the post deployment actions too.
 #### Optional stages
 The following stages are available as optional stages in this pipeline template:
 1. **Notification**: This stage will always run to notify (now just by email, but other channels can be added) the pipeline execution.
 status and possible details.

 #### Graphic visualization
 The below image shows you how the pipeline looks when you implement the delphinus_template and add optional stages too.
 ![alt text](img/delphinus_template.png "default view adding Notification stage")

#### Template Implementation
To use this template as your pipeline template, just follow the next steps:

 1. Be sure that your git project includes a **pipeline_config.groovy** file at the root level.
 2. Add the following code to your **pipeline_config.groovy** file:
 ```groovy
jte {
    pipeline_template = 'delphinus_template'
} 
 ```

Below is an example (based on a gradle app) of how an application can implement the **Delphinus template** using its **pipeline_config.groovy** fille.

```groovy
jte {
    pipeline_template = 'delphinus_template'
}

libraries {
    gradle
    ansible {
        deployment_type = 'app_center'
    }
    notification
}

application_environments {
    test {
        notify_to = ['developer@popular.com', 'tester@popular.com']
    }
}
```

#### Template libraries
This template handles Maven and Gradle-based projects.<br/> 
You need to specify your preference adding the 
maven or gradle library to your **pipeline_config.groovy** file as follow: 
 ```groovy
libraries {
    gradle
}
```
 If you need to use maven, just remove gradle and add maven.
 #### Template optional libraries
 Some libraries are optional to pass to your **pipeline_config.groovy** file. For example,
 you can add the **notification** library to activate the optional pipeline **Notification** stage as follow.<br/>

```groovy
libraries {
    gradle
    notification
}
```

</details>