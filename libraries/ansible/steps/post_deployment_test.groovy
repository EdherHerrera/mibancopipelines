void call() {
    String deployment_type = config.deployment_type ?: 'app_center'
    app_env = stageContext.args.app_env
    stage('Post Deployment Tests') {
        print_message("Post deployment tests from the $hookContext.library  library to $app_env.short_name environment ($deployment_type)")
    }
}

private void print_message(String message) {
    println message
}
