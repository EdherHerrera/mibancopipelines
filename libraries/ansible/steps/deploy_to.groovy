void call() {
    String deployment_type = config.deployment_type ?: ''
    app_env = stageContext.args.app_env
    stage("Deploy To $app_env.short_name") {
        print_message("Deployment from the $hookContext.library library to $app_env.short_name environment ($deployment_type)")
        print_message("Source branch name: $app_env.branch_name")
    }
}

private void print_message(String message) {
    println message
}
