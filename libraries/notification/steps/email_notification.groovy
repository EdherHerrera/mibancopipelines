void call() {
    app_env = stageContext.args.app_env
    stage('Notification') {
        println "Notifying pipeline status to: $app_env.notify_to from notification library ..."
    }
}
