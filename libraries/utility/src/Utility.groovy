package util

/**
* Utility class
* This class can be shared across multiple JTE libraries.
* @See: https://jenkinsci.github.io/templating-engine-plugin/2.5/concepts/library-development/library-classes/
* See warning in previous link.
*/
class Utility implements Serializable {

    void sayHello() {
        println 'hello from Utility class!'
    }

}
